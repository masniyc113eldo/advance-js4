const url = ' https://ajax.test-danit.com/api/swapi/films';
const filmsList = document.querySelector('#films-list');

fetch(url)
      .then(response => response.json())
      .then(data => {
        console.log(data)
        data.forEach(({ episodeId, name, openingCrawl, characters }) => {
          const filmElement = document.createElement('div');
          filmElement.innerHTML = `
            <h3>Episode ${episodeId}: ${name}</h3>
            <p>${openingCrawl}</p>
            <div id="characters-${episodeId}" class="loading-spinner"></div>
          `;

          filmsList.appendChild(filmElement);
          fetchCharacters(characters, episodeId);
        });
      })
      .catch(error => console.error('Error fetching films:', error));

   
    function fetchCharacters(characters, episodeId) {
      Promise.all(characters.map(url => fetch(url).then(response => response.json())))
        .then(charactersData => {
          const charactersContainer = document.querySelector(`#characters-${episodeId}`);
          charactersContainer.XMLHttpRequest = '<h4>Characters:</h4>';
          charactersData.forEach(({ name }) => {
            const characterName = document.createElement('p');
            characterName.textContent = name;
            charactersContainer.appendChild(characterName);
          });
        })
    }
